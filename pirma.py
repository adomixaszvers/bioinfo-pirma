# -*- coding: utf-8 -*-
import subprocess
import os.path
from shutil import copyfileobj, copy
from sys import maxsize

import Bio.Blast.NCBIWWW
import Bio.Blast.NCBIXML
from Bio.Align.Applications import MuscleCommandline
from Bio import AlignIO
from Bio import SeqIO
from Bio.Alphabet.IUPAC import protein
from Bio import Entrez
from Bio.Align import MultipleSeqAlignment


def sum25(x, y):
    return sum(x[y:y + 25]) / 25


def main():
    gi = '4504347'
    accession_number = "NP_000549.1"
    entrez_query = "mammals[Organizm]"

    try:
        if not os.path.exists("ncbi.xml"):
            print("Gaunami duomenys iš swissprot duomenų bazės. Gali tekti palaukti.")
            ncbi = Bio.Blast.NCBIWWW.qblast(program="blastp",
                                            database="swissprot",
                                            sequence=accession_number,
                                            entrez_query=entrez_query,
                                            expect=8,
                                            hitlist_size=1000,
                                            )
            with open("ncbi.xml", "w") as ncbi_handle:
                ncbi.seek(0)
                copyfileobj(ncbi, ncbi_handle)
    except KeyboardInterrupt:
        print("Kopijuojami senesni duomenų bazės duomenys.")
        copy("ncbi.xml.bak", "ncbi.xml")

    with open("ncbi.xml", "r") as ncbi_handle:
        blast = Bio.Blast.NCBIXML.read(ncbi_handle)

    with open("blast.fasta", "w") as aln:
        for sequence in blast.alignments:
            if "HB" in sequence.title and "Hemoglobin subunit alpha" in sequence.title:
                aln.write(">%s\n" % sequence.title)
                aln.write("%s\n" % sequence.hsps[0].sbjct)

    muscle_cline = MuscleCommandline(input="blast.fasta", out="muscle.fasta")

    subprocess.call(str(muscle_cline), shell=True)

    alignments = AlignIO.read("muscle.fasta", "fasta", alphabet=protein)

    human_seq = None
    other_seqs = []

    for i in range(len(alignments)):
        if "HUMAN" in alignments[i].name:
            human_seq = alignments[i]
        else:
            other_seqs.append(alignments[i])

    other_seqs = MultipleSeqAlignment(other_seqs)

    min_concurrence = maxsize
    nearest_neighbours = []

    for i in range(len(other_seqs)):
        counter = 0
        for j in range(len(human_seq)):
            if other_seqs[i][j] != human_seq[j]:
                counter += 1
        if counter < min_concurrence:
            min_concurrence = counter
            nearest_neighbours = [other_seqs[i]]
        elif counter == min_concurrence:
            nearest_neighbours.append(other_seqs[i])

    if human_seq is None:
        raise Exception("Nėra žmogaus sekos")

    concurrences = []
    aln_len = len(other_seqs)

    for i in range(len(human_seq)):
        amino_accid = human_seq[i]
        concurrence = other_seqs[:, i].count(amino_accid)
        concurrences.append(concurrence / aln_len)

    min_i, max_i = 0, 0
    min_sum, max_sum = sum25(concurrences, 0), sum25(concurrences, 0)

    seq_len = len(human_seq)

    for i in range(1, seq_len - 24):
        sum_i = sum25(concurrences, i)
        if sum_i < min_sum:
            min_sum = sum_i
            min_i = i
        if sum_i > max_sum:
            max_sum = sum_i
            max_i = i

    Entrez.email = "adomixaszvers@gmail.com"
    print("Panašiausias gyvūnai:")
    for seq in nearest_neighbours:
        handle = Entrez.efetch(db="nucleotide", id=seq.id, rettype="gb", retmode="xml")
        res = Entrez.read(handle)
        print(res[0]["GBSeq_organism"])

    print("******************************")
    print("Nuo", min_i, "iki", min_i + 24, "minimalus", min_sum, "sutapimas")
    print(human_seq[min_i:min_i + 25])
    SeqIO.write(human_seq[min_i:min_i + 25], "human.fasta", "fasta")

    print("Nuo", max_i, "iki", max_i + 24, "maksimalus", max_sum, "sutapimas")
    print(human_seq[max_i:max_i + 25])
    SeqIO.write(human_seq[max_i:max_i + 24], "mammals.fasta", "fasta")


if __name__ == "__main__":
    main()
